from pymongo import MongoClient
import json
import re
import logging
from env import *

"""
DB CONNECTION
"""
client = None
def dbconn():
	global client
	if client is None:
		client = MongoClient(DB_URL,serverSelectionTimeoutMS=60000, connectTimeoutMS=20000 ,socketTimeoutMS=45000, maxPoolSize =1000)
		logging.info("==========mongo database is connected correctly===========")
	return client


def set_slot(sender_id,slot_name,slot_value):
	client= dbconn()
	db = client[DB_NAME]
	coll = db.slots
	coll.update({"sender_id":sender_id},{"$set":{slot_name:slot_value}},True)
	client.close()
	return None

def set_multiple_slots(sender_id,slots):
	client = dbconn()
	db = client[DB_NAME]
	coll = db.slots
	coll.update({"sender_id":sender_id},{"$set":slots},True)
	client.close()
	return None

def get_slot(sender_id,slot_name):
	client = dbconn()
	db = client[DB_NAME]
	coll = db.slots
	slot_value = None
	result = coll.find_one({"sender_id":sender_id})
	if result is not None and slot_name in result:
		slot_value = result[slot_name]
	client.close()
	return slot_value

